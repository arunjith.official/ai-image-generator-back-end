import express from "express";
import * as dotenv from "dotenv";
import { Configuration, OpenAIApi } from "openai";

dotenv.config();

const router = express.Router();

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

router.route("/").get((req, res) => {
  res.status(200).json({ message: "Hello from DALL-E!" });
});

const openaiConfig = {
  n: 1,
  size: "1024x1024",
  response_format: "b64_json",
};

router.post("/", async (req, res) => {
  const { prompt } = req.body;

  if (!prompt) {
    return res.status(400).json({
      error: "Missing 'prompt' property in request body.",
    });
  }

  try {
    const aiResponse = await openai.createImage({
      ...openaiConfig,
      prompt,
    });
  
    const image = aiResponse?.data?.data?.[0]?.b64_json;

    if (image) {
      return res.status(200).json({ photo: image });
    } else {
      return res.status(500).json({
        error: "Could not retrieve image from AI response.",
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      error:
        error?.response?.data?.error?.message || "An unknown error occurred.",
    });
  }
});
export default router;
